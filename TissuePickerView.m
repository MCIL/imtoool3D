classdef TissuePickerView < handle
% TissuePickerView  a GUI representation of the TissuePickerModel
%
% Copyright (C) 2016-2022    Michigan State University Board of Trustees
% Author:   Matt Latourette

    %% Properties

    % Observable Properties (listeners receive notification of changes
    properties (SetObservable = true, AbortSet = true)
        Model
        UiControls
    end

    % Dependent properties
    properties (SetObservable = true, AbortSet = true, Dependent = true)
    end

    % Private Properties
    properties (Access = private)
    end

    % Private Computable Dependent Properties
    properties (Dependent = true, SetAccess = private)
    end

    %% Events
    events
    end

    %% Public Methods
    methods
        %% Constructors
        function this = TissuePickerView(model)
            this.Model = model;

            this.InitializeGui();
            this.TriggerUiControlInitialization();
            this.RegisterEventListeners();
        end

        %% Getters for Computable Dependent Properties

        %% Getters and Setters
        function out = get.Model(this)
            out = this.Model;
        end

        function set.Model(this, value)
            this.Model = value;
        end

        %% Other Public Methods
    end

    %% Private Methods
    methods (Access = private)
        %% TriggerUiControlInitialization
        function TriggerUiControlInitialization(this)
            model = this.Model;
            this.OnChangedTissueType(model);
        end

        %% RegisterEventListeners
        function RegisterEventListeners(this)
            model = this.Model;
            addlistener(model, 'TissueType', 'PostSet', @(o,e) OnChangedTissueType(this, e.AffectedObject));
        end

        %% InitializeGui
        function InitializeGui(this)
            model = this.Model;
            tissueType = model.TissueType;

            hFig = figure('Visible', 'off', 'Name', 'Set the Tissue Type', 'NumberTitle', 'off', ...
                'Position', [200 200 326 150], 'WindowStyle', 'modal');

            hTissueTypePanel = uipanel(hFig, 'Title', 'Select a Tissue Type', 'Visible', 'off', ...
                'Units', 'pixels', 'Position', [12 55 304 85]);
            cdata = imread('liver-icon-48-px.png');
            hLiverButton = uicontrol(hTissueTypePanel, 'Style', 'pushbutton', 'CData', cdata, 'String', '', ...
                'TooltipString', 'Liver', 'Position', [8 12 54 54]);
            cdata = imread('spleen-icon-48-px.png');
            hSpleenButton = uicontrol(hTissueTypePanel, 'Style', 'pushbutton', 'CData', cdata, 'String', '', ...
                'TooltipString', 'Spleen', 'Position', [66 12 54 54]);
            cdata = imread('kidney-icon-48-px.png');
            hKidneyButton = uicontrol(hTissueTypePanel, 'TooltipString', 'Kidney', 'Style', 'pushbutton', ...
                'CData', cdata, 'String', '', 'Position', [124 12 54 54]);
            cdata = imread('arterial-blood-icon-48-px.png');
            hArterialBloodButton = uicontrol(hTissueTypePanel, 'Style', 'pushbutton', 'CData', cdata, 'String', '', ...
                'TooltipString', 'Arterial Blood', 'Position', [182 12 54 54]);
            cdata = imread('venous-blood-icon-48-px.png');
            hVenousBloodButton = uicontrol(hTissueTypePanel, 'Style', 'pushbutton', 'CData', cdata, 'String', '', ...
                'TooltipString', 'Venous Blood', 'Position', [242 12 54 54]);

            hCustomTypeLabel = uicontrol(hFig, 'Style', 'text', 'String', 'Custom Type', ...
                'HorizontalAlignment', 'left', 'Position', [12 37 100 13]);
            hCustomTypeEditBox = uicontrol(hFig, 'Style', 'edit', 'String', tissueType, 'Position', [12 11 100 22]);
            hOkButton = uicontrol('Parent', hFig, 'Style', 'pushbutton', 'String', 'OK', 'Position', [174 11 65 22]);
            hCancelButton = uicontrol('Parent', hFig, 'Style', 'pushbutton', 'String', 'Cancel', ...
                'Position', [252 11 65 22]);

            this.UiControls = struct(...
                'Figure', hFig, ...
                'TissueTypeButtonGroup', hTissueTypePanel, ...
                'LiverButton', hLiverButton, ...
                'SpleenButton', hSpleenButton, ...
                'KidneyButton', hKidneyButton, ...
                'ArterialBloodButton', hArterialBloodButton, ...
                'VenousBloodButton', hVenousBloodButton, ...
                'CustomTypeLabel', hCustomTypeLabel, ...
                'CustomTypeEditBox', hCustomTypeEditBox, ...
                'OkButton', hOkButton, ...
                'CancelButton', hCancelButton);

            this.NormalizeDisplayUnits();
            this.MoveToPosition();
            this.MakeVisible();
        end

        %% MakeVisible
        function MakeVisible(this)
            this.UiControls.TissueTypeButtonGroup.Visible = 'on';
            this.UiControls.Figure.Visible = 'on';
        end

        %% MoveToPosition
        function MoveToPosition(this)
            model = this.Model;
            savedScreenPosition = model.SavedScreenPosition;
            if(isempty(savedScreenPosition))
                % Move the GUI to the screen center
                movegui(this.UiControls.Figure, 'center');
            else
                movegui(this.UiControls.Figure, savedScreenPosition);
                movegui(this.UiControls.Figure, 'onscreen');
            end
        end

        %% NormalizeDisplayUnits
        function NormalizeDisplayUnits(this)
            controlsToExclude = {};
            uiControlNames = fieldnames(this.UiControls);
            numberOfUiControls = numel(uiControlNames);

            if (numberOfUiControls > 0)
                for controlIndex = 1:numberOfUiControls
                    controlName = uiControlNames{controlIndex};
                    if (~any(strcmp(controlsToExclude, controlName)))
                        uiControl = getfield(this.UiControls, controlName);
                        uiControl.Units = 'normalized';
                    end
                end
            end
        end

        %% OnChangedTissueType
        function OnChangedTissueType(this, model)
        end
    end
end

