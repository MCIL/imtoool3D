classdef TissuePickerController < handle
% TissuePickerController
%
% Copyright (C) 2016-2022    Michigan State University Board of Trustees
% Author:   Matt Latourette

    %% Properties

    % Observable Properties (listeners receive notification of changes)
    properties (SetObservable = true, AbortSet = true)
        Model
        View
    end

    % Dependent properties
    properties (SetObservable = true, AbortSet = true, Dependent = true)
    end

    % Private Properties
    properties (Access = private)
    end

    % Private Computable Dependent Properties
    properties (Dependent = true, SetAccess = private)
    end

    %% Events
    events
    end

    %% Public Methods
    methods
        %% Constructors
        function this = TissuePickerController(tissueType)
            model = TissuePickerModel(tissueType);
            view = TissuePickerView(model);
            this.Model = model;
            this.View = view;

            this.RegisterUiEventHandlers();

            uiwait(view.UiControls.Figure);
            close(view.UiControls.Figure);

            if (model.Cancelled)
                this = TissuePickerController.empty;
            end
        end

        %% Getters for Computable Dependent Properties

        %% Getters and Setters

        %% Other Public Methods
    end

    %% Private Methods
    methods (Access = private)
        function RegisterUiEventHandlers(this)
            model = this.Model;
            uiControls = this.View.UiControls;

            % respond to the view's events
            set(uiControls.LiverButton, 'Callback', ...
                {@TissuePickerController.OnTissueTypeButton_Press, model, 'liver'});
            set(uiControls.SpleenButton, 'Callback', ...
                {@TissuePickerController.OnTissueTypeButton_Press, model, 'spleen'});
            set(uiControls.KidneyButton, 'Callback', ...
                {@TissuePickerController.OnTissueTypeButton_Press, model, 'kidney'});
            set(uiControls.ArterialBloodButton, 'Callback', ...
                {@TissuePickerController.OnTissueTypeButton_Press, model, 'arterial blood'});
            set(uiControls.VenousBloodButton, 'Callback', ...
                {@TissuePickerController.OnTissueTypeButton_Press, model, 'venous blood'});
            set(uiControls.CustomTypeEditBox, 'Callback', {@TissuePickerController.OnCustomType_Edit, model});
            set(uiControls.OkButton, 'Callback', {@TissuePickerController.OnOkButton_Press, model});
            set(uiControls.CancelButton, 'Callback', {@TissuePickerController.OnCancelButton_Press, model});
        end
    end

    %% Public Static Methods
    methods (Static)
        %% OnTissueTypeButton_Press
        function OnTissueTypeButton_Press(o, ~, model, tissueType)
            model.TissueType = tissueType;
            model.Cancelled = false;
            uiresume(o.Parent.Parent);
        end

        %% OnCustomType_Edit
        function OnCustomType_Edit(o, ~, model)
            str = get(o, 'String');
            model.TissueType = str;
        end

        %% OnOkButton_Press
        function OnOkButton_Press(o, ~, model)
            model.Cancelled = false;
            uiresume(o.Parent);
        end

        %% OnCancelButton_Press
        function OnCancelButton_Press(o, ~, model)
            model.Cancelled = true;
            uiresume(o.Parent);
        end
    end

    %% Private Static Methods
    methods (Static, Access = private)
    end
end