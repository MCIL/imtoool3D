classdef TissuePickerModel < handle
% TissuePickerModel
%
% Copyright (C) 2016-2022    Michigan State University Board of Trustees
% Author:   Matt Latourette
    
    %% Properties
    
    % Observables (listeners receive notification of changes)
    properties (SetObservable = true, AbortSet = true)
        TissueType
        SavedScreenPosition
        Cancelled(1,1) logical
    end
    
    % Read-only properties
    properties (SetAccess = private)
    end
    
    % Private properties
    properties (Access = private)
    end
    
    % Computable dependent properties
    properties (Dependent = true, SetAccess = private)
    end
    
    %% Events
    events
    end
    
    %% Class Methods
    methods
        %% Constructors
        function this = TissuePickerModel(tissueType)
            % Constructor
            this.TissueType = tissueType;
            this.SavedScreenPosition = '';
        end

        %% Getters for Computable Dependent Properties
        
        %% Getters and Setters
        function out = get.TissueType(this)
            out = this.TissueType;
        end
        
        function set.TissueType(this, value)
            this.TissueType = value;
        end

        function out = get.SavedScreenPosition(this)
            out = this.SavedScreenPosition;
        end
        
        function set.SavedScreenPosition(this, value)
            this.SavedScreenPosition = value;
        end

        %% Other Public Methods
    end

    %% Private Methods
    methods (Access = private)
    end

    %% Public Static Methods
    methods (Static)
    end

    %% Private Static Methods
    methods (Static, Access = private)
    end
end